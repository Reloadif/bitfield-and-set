#include "TSet.h"

void showBFMenu() {
	using namespace std;
	system("cls");
	cout << "Select the menu item :" << endl;
	cout << "1. Switch on bit." << endl;
	cout << "2. Switch off bit." << endl;
	cout << "3. Get result of the bit." << endl;
	cout << "4. Invers bit field." << endl;
	cout << "5. Bit and from bit field." << endl;
	cout << "6. Bit or from bit field." << endl;
	cout << "7. Compare two bits field." << endl;
	cout << "8. Output information." << endl;
	cout << "9. Go to Set menu with this bitfield." << endl << endl;
	cout << "   <Press 0 to exit the program>" << endl << endl;
}
void inputBFel(UI& l, TBitField& a) {
	using namespace std;
	do {
		system("cls");
		cout << "Input number of the bit: ";
		cin >> l;
	} while (l < 0 || l > a.getCoBit());
	system("cls");
}
void showTSetMenu() {
	using namespace std;
	system("cls");
	cout << "Select the menu item :" << endl;
	cout << "1. Include element." << endl;
	cout << "2. Disable element." << endl;
	cout << "3. Get result of the element." << endl;
	cout << "4. Invers set." << endl;
	cout << "5. Bit and from set." << endl;
	cout << "6. Bit or from set." << endl;
	cout << "7. Output information." << endl << endl;
	cout << "   <Press 0 to exit the program>" << endl << endl;
}
void inputTSetEl(UI& l , TSet& a) {
	using namespace std;
	do {
		system("cls");
		cout << "Enter number of element in set: ";
		cin >> l;
	} while (l < 0 || l > a.getMaxPower() );
	system("cls");
}

int main() {
	using namespace std;
	
	int choose;
	UI lamp;

	TBitField Bneed;
	TBitField Bmain;
	cin >> Bmain;
	cout << Bmain;

	do {
		showBFMenu();
		cin >> choose;
		switch (choose) {
		case 0:
			system("cls");
			cout << "Goodbye!";
			break;
		case 1:
			system("cls");
			inputBFel(lamp, Bmain);
			Bmain.setBit(lamp - 1);
			cout << Bmain;
			break;
		case 2:
			system("cls");
			inputBFel(lamp, Bmain);
			Bmain.delBit(lamp - 1);
			cout << Bmain;
			break;
		case 3:
			system("cls");
			inputBFel(lamp, Bmain);

			cout << "Lamp[" << lamp << "] is ";
			if (Bmain.getBit(lamp - 1)) cout << "on." << endl;
			else cout << "off." << endl;

			system("pause");
			break;
		case 4:
			system("cls");
			!Bmain;
			cout << Bmain;
			break;
		case 5:
			system("cls");
			cin >> Bneed;
			Bmain = Bmain & Bneed;
			cout << Bmain;
			break;
		case 6:
			system("cls");
			cin >> Bneed;
			Bmain = Bmain | Bneed;
			cout << Bmain;
			break;
		case 7:
			system("cls");
			cin >> Bneed;
			system("cls");

			if (Bmain == Bneed) {
				cout << "True" << endl;
			}
			else cout << "False" << endl;

			system("pause");
			break;
		case 8:
			system("cls");
			cout << Bmain;
			break;
		case 9:
			system("cls");
			goto B;
			break;
		default:
			system("cls");
			cout << "You entered an invalid menu item number, available values are from 0 to 9." << endl << endl;
			system("pause");
		}
	} while (choose);


B:
	TSet  Tneed;
	TSet Tmain(Bmain);

	cout << Tmain;

	do {
		showTSetMenu();
		cin >> choose;
		switch (choose) {
		case 0:
			system("cls");
			cout << "Goodbye!";
			break;
		case 1:
			system("cls");
			inputTSetEl(lamp, Tmain);
			Tmain.addElem(lamp - 1);
			cout << Tmain;
			break;
		case 2:
			system("cls");
			inputTSetEl(lamp, Tmain);
			Tmain.delElem(lamp - 1);
			cout << Tmain;
			break;
		case 3:
			system("cls");
			inputTSetEl(lamp, Tmain);

			cout << "Element " << lamp << " is ";
			if (Tmain.getElem(lamp - 1)) cout << "included." << endl;
			else cout << "not included." << endl;

			system("pause");
			break;
		case 4:
			system("cls");
			!Tmain;
			cout << Tmain;
			break;
		case 5:
			system("cls");
			cin >> Tneed;
			Tmain = Tmain - Tneed;
			cout << Tmain;
			break;
		case 6:
			system("cls");
			cin >> Tneed;
			Tmain = Tmain + Tneed;
			cout << Tmain;
			break;
		case 7:
			system("cls");
			cout << Tmain;
			break;
		default:
			system("cls");
			cout << "You entered an invalid menu item number, available values are from 0 to 7." << endl << endl;
			system("pause");
		}
	} while (choose);
	

	return 0;
}
