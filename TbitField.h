#ifndef _TBITFIELD_
#define _TBITFIELD_

#include <iostream>

typedef unsigned int UI;

class TBitField {
	UI CoBit;
	UI SiArray;
	UI* BitArray;

	UI bitMask(const UI bit) const;
	UI indexArr(const UI bit) const;
public:
	TBitField();
	TBitField(UI _CoBit);
	TBitField(const TBitField& a);

	~TBitField();

	UI getCoBit(void) const;

	void setBit(const UI bit);
	void delBit(const UI bit);
	bool getBit(const UI bit);

	bool operator==(const TBitField& a) const;
	TBitField& operator=(const TBitField& a);

	TBitField& operator!();
	TBitField& operator&(const TBitField& a);
	TBitField& operator|(const TBitField& a);


	friend std::istream& operator >> (std::istream& stream, TBitField& obj);
	friend std::ostream& operator << (std::ostream& stream, const TBitField& obj);
};

#endif // !_TBITFIELD_